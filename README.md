# RLNET.Core #

### What is this repository for? ###

* This repository is the home of the .NET Core port of [RLNET](https://bitbucket.org/clarktravism/rlnet/overview). A lightweight easy to use API for creating tile based games.  
RLNET.Core provides a console output, swappable colors, true color sprites, as well as keyboard and mouse inputs. RLNET was created by Travis M. Clark.

* The current version of RLNET.Core is 1.0.0 (based off RLNET 1.0.6)

### How do I get set up? ###

* (TODO) Summary of set up
* (TODO) Dependencies

### Who do I talk to? ###

* Questions, comments and concerns can be addressed to Samuel Henderson